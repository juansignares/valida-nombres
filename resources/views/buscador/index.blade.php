@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-4">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Buscador</div>

                <div id="card-body" class="card-body">
                    <panel-busqueda></panel-busqueda>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection