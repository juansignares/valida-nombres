<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1',
    'middleware' => 'apikey'
], function () {
    Route::group([
        'prefix' => 'auth'
        ], function () {

        Route::post('login', '\App\Http\Controllers\Api\V1\AuthController@login')->name('api.login');
        Route::post('signup', '\App\Http\Controllers\Api\V1\AuthController@signUp')->name('api.signup');

        Route::group([
            'middleware' => 'auth:api'
            ], function() {
            Route::get('logout', '\App\Http\Controllers\Api\V1\AuthController@logout')->name('api.logout');
            Route::get('user', '\App\Http\Controllers\Api\V1\AuthController@user')->name('api.user');
        });
    });

    Route::group([
        'prefix' => 'config'
        ], function () {
        Route::get('updatePersonas', '\App\Http\Controllers\Api\V1\BasePersonasController@updatePersonas')->name('api.updatePersonas');
    });

    Route::group([
        'prefix' => 'busqueda',
        'middleware' => ['auth:api']
        ], function () {
        Route::post('nueva', '\App\Http\Controllers\Api\V1\BusquedaController@nueva')->name('api.nuevabusqueda');
        Route::get('/{uuid}', '\App\Http\Controllers\Api\V1\BusquedaController@recuperar')->name('api.recuperarbusqueda');
    });
});