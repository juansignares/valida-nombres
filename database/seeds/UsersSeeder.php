<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Juano Lorica',
                'email' => 'ejemplo@yopmail.com',
                'password' => Hash::make('123456'),
            ],
        ]);
    }
}
