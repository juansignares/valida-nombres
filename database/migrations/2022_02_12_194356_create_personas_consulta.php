<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasConsulta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas_consulta', function (Blueprint $table) {
            $table->id();
            $table->foreignId('consultas_id');
            $table->foreignId('diccionario_id');
            $table->double('ptj_coincidencia', 4, 2);
            //
            $table->foreign('consultas_id')->references('id')->on('consultas')->onDelete('restrict');
            $table->foreign('diccionario_id')->references('id')->on('diccionario')->onDelete('restrict');
            //
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas_consulta');
    }
}
