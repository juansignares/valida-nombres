<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDiccionario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diccionario', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->integer('anos_activo');
            $table->foreignId('departamentos_id');
            $table->foreignId('localidades_id');
            $table->foreignId('municipios_id');
            $table->foreignId('tipos_persona_id');
            $table->foreignId('tipos_cargo_id');
            //
            $table->foreign('departamentos_id')->references('id')->on('departamentos')->onDelete('restrict');
            $table->foreign('localidades_id')->references('id')->on('localidades')->onDelete('restrict');
            $table->foreign('municipios_id')->references('id')->on('municipios')->onDelete('restrict');
            $table->foreign('tipos_persona_id')->references('id')->on('tipos_persona')->onDelete('restrict');
            $table->foreign('tipos_cargo_id')->references('id')->on('tipos_cargo')->onDelete('restrict');
            //
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diccionario');
    }
}
