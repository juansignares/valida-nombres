# Descripción:
Aplicación en Laravel 7.29
PHP 7.3.7
MariaDB 10.3.16

## Despliegue en local:

# 0. Modificar el archivo .env.example > .env y especificar Base de datos
    php artisan key:generate

# 1. Instalación de paquetes (de ser necesario)

    npm install
    composer install

# 2. Limpieza de caché y generación de nuevos archivos caché

    php artisan optimize
    composer dump-autoload
    npm run dev

# 3. Montaje de base de datos
    
    php artisan migrate
    php artisan db:seed
    php artisan passport:install

# 4. Procesos en terminal para que funcione en local:

    php artisan serve

# Sólo si se va a usar el endpoint /api/v1/config/updatePersonas se deben tener corriendo estos dos comandos en terminales distintas:
## Hacer esto al menos una vez en local para contar con la base de datos completa
    php artisan serve
    php artisan queue:work database --queue procesadorUploadPersonas --tries=1





##### Mapeo de API:

## HEADERS generales para todas las peticiones:
    Content-Type: application/json
    X-Requested-With: XMLHttpRequest
# key personalizada, sin esta no se acceder a ningún EndPoint del Web Service.
    api-key: WlskIyHmkUfMsyjkTksh



### ENDPOINTS AUTENTICACIÓN------------------------


## 1. /api/v1/auth/signup (POST)
# Body parámetros:
    - "name"
    - "email"
    - "password"
# Ejemplo:
    {
        "name": "Juano Lorica",
        "email": "ejemplo@yopmail.com",
        "password": "123456"
    }

# Retorno (ejemplo):
    {
        "message": "Usuario creado correctamente!"
    }
#

## 2. /api/v1/auth/login (POST)
# Body parámetros:
    - "email"
    - "password"
    - "remember_me": true|null
# Ejemplo:
    {
        "email": "ejemplo@yopmail.com",
        "password": "123456"
    }

# Retorno (ejemplo):
    {
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNzMwYzk3NzBhN2Y5NTVlNmZiMTc0ZDdmYjBjZmVhMzQ4NTRmOTliYjhkYTYxYWY2ZDRhMjIwMzQ0MjJlNjc1M2VhOWE0OWMzYzhkZmQyYjMiLCJpYXQiOjE2NDAyNzk3MzksIm5iZiI6MTY0MDI3OTczOSwiZXhwIjoxNjcxODE1NzM5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.eQpDi0c7zBQknXyQHZS1IqI8I3hRrPmgjxsdnRy0dNPgUQgYyQu0OWhkHARdOzzzl3SQZuihqvVAq7kqYpjiaalyPGPcBp4EyVDAeWwp9mPVNyc6oIgxJJOl6ZA4JCWoXu2PFo3fw8rWoK-KQ-yn1_y2lMgp5l88StTmTDGDOj1aiEqTFceFLajRCFABfAQW0sbgdcHpaTVJjx8vGi8JlMlt07NvEUFmoYwIJdRnMtbHSzNrbs8e4J6sNqJFeIOAjFPutIaUG5lg58Kw7ly-TlS1eJBrxOROC-g5WP1rHskOcv_fCAWfK_8mVgW29r7_OdpQc87lccm-B_C4pgw439XB-U2qkm-S1P8XZZKbVG09vyalQyCMbUG5wUbBst8rFp9ka0aVGKhBAaPdc8hi5vPEYMkNrHbsg2mf3J68sp11qKWLsqsW_HqHyZJCmrtn85tIFDk8Xs8K9g-3mgdmne2Wtdf1KIfHbb4r47wQMJj15vVkiralsIpjff8TarswAxYAiYZDBpWxj1zV8ZnolwuharNBcnrakWEYFoNRVHfkMOBjZ87Mjqmnl-MFAdkfKVXV1VA3PyTcY3ebTi6eo4xyBNxTgz4cg1o0IaALZqARIkVayKpp3AUB5WquvmyGLH9R7Jzjl5QaoaPVuSlQ_Dci1QqnYtZ5Igk56LcPEiA",
        "token_type": "Bearer",
        "expires_at": "2022-12-23 17:15:39"
    }
#

## 3. /api/v1/auth/user (GET)
# Headers adicionales:
    - Authorization: "token_type" "access_token"
# Ejemplo:
    Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNzMwYzk3NzBhN2Y5NTVlNmZiMTc0ZDdmYjBjZmVhMzQ4NTRmOTliYjhkYTYxYWY2ZDRhMjIwMzQ0MjJlNjc1M2VhOWE0OWMzYzhkZmQyYjMiLCJpYXQiOjE2NDAyNzk3MzksIm5iZiI6MTY0MDI3OTczOSwiZXhwIjoxNjcxODE1NzM5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.eQpDi0c7zBQknXyQHZS1IqI8I3hRrPmgjxsdnRy0dNPgUQgYyQu0OWhkHARdOzzzl3SQZuihqvVAq7kqYpjiaalyPGPcBp4EyVDAeWwp9mPVNyc6oIgxJJOl6ZA4JCWoXu2PFo3fw8rWoK-KQ-yn1_y2lMgp5l88StTmTDGDOj1aiEqTFceFLajRCFABfAQW0sbgdcHpaTVJjx8vGi8JlMlt07NvEUFmoYwIJdRnMtbHSzNrbs8e4J6sNqJFeIOAjFPutIaUG5lg58Kw7ly-TlS1eJBrxOROC-g5WP1rHskOcv_fCAWfK_8mVgW29r7_OdpQc87lccm-B_C4pgw439XB-U2qkm-S1P8XZZKbVG09vyalQyCMbUG5wUbBst8rFp9ka0aVGKhBAaPdc8hi5vPEYMkNrHbsg2mf3J68sp11qKWLsqsW_HqHyZJCmrtn85tIFDk8Xs8K9g-3mgdmne2Wtdf1KIfHbb4r47wQMJj15vVkiralsIpjff8TarswAxYAiYZDBpWxj1zV8ZnolwuharNBcnrakWEYFoNRVHfkMOBjZ87Mjqmnl-MFAdkfKVXV1VA3PyTcY3ebTi6eo4xyBNxTgz4cg1o0IaALZqARIkVayKpp3AUB5WquvmyGLH9R7Jzjl5QaoaPVuSlQ_Dci1QqnYtZ5Igk56LcPEiA


# Retorno (ejemplo):
    {
        "id": 1,
        "name": "Juano Lorica",
        "email": "ejemplo@yopmail.com",
        "email_verified_at": null,
        "created_at": "2021-12-23T17:00:00.000000Z",
        "updated_at": "2021-12-23T17:00:00.000000Z"
    }
#

## 4. /api/v1/auth/logout (GET)
# Headers adicionales:
    - Authorization: "token_type" "access_token"
# Ejemplo:
    Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNzMwYzk3NzBhN2Y5NTVlNmZiMTc0ZDdmYjBjZmVhMzQ4NTRmOTliYjhkYTYxYWY2ZDRhMjIwMzQ0MjJlNjc1M2VhOWE0OWMzYzhkZmQyYjMiLCJpYXQiOjE2NDAyNzk3MzksIm5iZiI6MTY0MDI3OTczOSwiZXhwIjoxNjcxODE1NzM5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.eQpDi0c7zBQknXyQHZS1IqI8I3hRrPmgjxsdnRy0dNPgUQgYyQu0OWhkHARdOzzzl3SQZuihqvVAq7kqYpjiaalyPGPcBp4EyVDAeWwp9mPVNyc6oIgxJJOl6ZA4JCWoXu2PFo3fw8rWoK-KQ-yn1_y2lMgp5l88StTmTDGDOj1aiEqTFceFLajRCFABfAQW0sbgdcHpaTVJjx8vGi8JlMlt07NvEUFmoYwIJdRnMtbHSzNrbs8e4J6sNqJFeIOAjFPutIaUG5lg58Kw7ly-TlS1eJBrxOROC-g5WP1rHskOcv_fCAWfK_8mVgW29r7_OdpQc87lccm-B_C4pgw439XB-U2qkm-S1P8XZZKbVG09vyalQyCMbUG5wUbBst8rFp9ka0aVGKhBAaPdc8hi5vPEYMkNrHbsg2mf3J68sp11qKWLsqsW_HqHyZJCmrtn85tIFDk8Xs8K9g-3mgdmne2Wtdf1KIfHbb4r47wQMJj15vVkiralsIpjff8TarswAxYAiYZDBpWxj1zV8ZnolwuharNBcnrakWEYFoNRVHfkMOBjZ87Mjqmnl-MFAdkfKVXV1VA3PyTcY3ebTi6eo4xyBNxTgz4cg1o0IaALZqARIkVayKpp3AUB5WquvmyGLH9R7Jzjl5QaoaPVuSlQ_Dci1QqnYtZ5Igk56LcPEiA

# Retorno (ejemplo):
    {
        "message": "Has cerrado sesión"
    }
#




### ENDPOINTS CONFIGURACION------------------------

## 5. /api/v1/config/updatePersonas (GET)
Proceso asincrónico sencillo para actualizar la base de datos con el archivo CSV
(Este proceso puede tomar unos minutos la primera vez que se ejecute)

# Retorno (ejemplo):
    {
        "code": 200,
        "message": "Se inició proceso de actualización."
    }
#




### ENDPOINTS BUSQUEDA------------------------

## 6. /api/v1/busqueda (POST)
# Headers adicionales:
    - Authorization: "token_type" "access_token"
# Ejemplo:
    Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNzMwYzk3NzBhN2Y5NTVlNmZiMTc0ZDdmYjBjZmVhMzQ4NTRmOTliYjhkYTYxYWY2ZDRhMjIwMzQ0MjJlNjc1M2VhOWE0OWMzYzhkZmQyYjMiLCJpYXQiOjE2NDAyNzk3MzksIm5iZiI6MTY0MDI3OTczOSwiZXhwIjoxNjcxODE1NzM5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.eQpDi0c7zBQknXyQHZS1IqI8I3hRrPmgjxsdnRy0dNPgUQgYyQu0OWhkHARdOzzzl3SQZuihqvVAq7kqYpjiaalyPGPcBp4EyVDAeWwp9mPVNyc6oIgxJJOl6ZA4JCWoXu2PFo3fw8rWoK-KQ-yn1_y2lMgp5l88StTmTDGDOj1aiEqTFceFLajRCFABfAQW0sbgdcHpaTVJjx8vGi8JlMlt07NvEUFmoYwIJdRnMtbHSzNrbs8e4J6sNqJFeIOAjFPutIaUG5lg58Kw7ly-TlS1eJBrxOROC-g5WP1rHskOcv_fCAWfK_8mVgW29r7_OdpQc87lccm-B_C4pgw439XB-U2qkm-S1P8XZZKbVG09vyalQyCMbUG5wUbBst8rFp9ka0aVGKhBAaPdc8hi5vPEYMkNrHbsg2mf3J68sp11qKWLsqsW_HqHyZJCmrtn85tIFDk8Xs8K9g-3mgdmne2Wtdf1KIfHbb4r47wQMJj15vVkiralsIpjff8TarswAxYAiYZDBpWxj1zV8ZnolwuharNBcnrakWEYFoNRVHfkMOBjZ87Mjqmnl-MFAdkfKVXV1VA3PyTcY3ebTi6eo4xyBNxTgz4cg1o0IaALZqARIkVayKpp3AUB5WquvmyGLH9R7Jzjl5QaoaPVuSlQ_Dci1QqnYtZ5Igk56LcPEiA

# Body parámetros:
    - "busqueda"
    - "porcentaje"
# Ejemplo:
    {
        "busqueda": "Peres Fernandez",
        "porcentaje": "70.2"
    }

# Retorno (ejemplo):
    {
        "uuid": 7,
        "nombre_buscado": "Peres Fernandez",
        "porcentaje_buscado": 70.2,
        "estado_ejecucion": "Registros encontrados",
        "coincidencias": [
            {
                "id": 2,
                "nombre_encontrado": "Porfirio Perez Fernandez",
                "porcentaje_encontrado": 77,
                "otros_campos": {
                    "anos_activo": 11,
                    "departamento": "NORTE_DE_SANTANDER",
                    "localidad": "NO APLICA",
                    "municipio": "TOLEDO",
                    "tipo_persona": "PREFERENTE",
                    "tipo_cargo": "CANTANTE"
                }
            }
        ]
    }
#


## 7. /api/v1/busqueda/{uuid} (GET)
# Headers adicionales:
    - Authorization: "token_type" "access_token"
# Ejemplo:
    Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNzMwYzk3NzBhN2Y5NTVlNmZiMTc0ZDdmYjBjZmVhMzQ4NTRmOTliYjhkYTYxYWY2ZDRhMjIwMzQ0MjJlNjc1M2VhOWE0OWMzYzhkZmQyYjMiLCJpYXQiOjE2NDAyNzk3MzksIm5iZiI6MTY0MDI3OTczOSwiZXhwIjoxNjcxODE1NzM5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.eQpDi0c7zBQknXyQHZS1IqI8I3hRrPmgjxsdnRy0dNPgUQgYyQu0OWhkHARdOzzzl3SQZuihqvVAq7kqYpjiaalyPGPcBp4EyVDAeWwp9mPVNyc6oIgxJJOl6ZA4JCWoXu2PFo3fw8rWoK-KQ-yn1_y2lMgp5l88StTmTDGDOj1aiEqTFceFLajRCFABfAQW0sbgdcHpaTVJjx8vGi8JlMlt07NvEUFmoYwIJdRnMtbHSzNrbs8e4J6sNqJFeIOAjFPutIaUG5lg58Kw7ly-TlS1eJBrxOROC-g5WP1rHskOcv_fCAWfK_8mVgW29r7_OdpQc87lccm-B_C4pgw439XB-U2qkm-S1P8XZZKbVG09vyalQyCMbUG5wUbBst8rFp9ka0aVGKhBAaPdc8hi5vPEYMkNrHbsg2mf3J68sp11qKWLsqsW_HqHyZJCmrtn85tIFDk8Xs8K9g-3mgdmne2Wtdf1KIfHbb4r47wQMJj15vVkiralsIpjff8TarswAxYAiYZDBpWxj1zV8ZnolwuharNBcnrakWEYFoNRVHfkMOBjZ87Mjqmnl-MFAdkfKVXV1VA3PyTcY3ebTi6eo4xyBNxTgz4cg1o0IaALZqARIkVayKpp3AUB5WquvmyGLH9R7Jzjl5QaoaPVuSlQ_Dci1QqnYtZ5Igk56LcPEiA

# Retorno (ejemplo):
    {
        "uuid": 7,
        "nombre_buscado": "Peres Fernandez",
        "porcentaje_buscado": 70.2,
        "estado_ejecucion": "Registros encontrados",
        "coincidencias": [
            {
                "id": 2,
                "nombre_encontrado": "Porfirio Perez Fernandez",
                "porcentaje_encontrado": 77,
                "otros_campos": {
                    "anos_activo": 11,
                    "departamento": "NORTE_DE_SANTANDER",
                    "localidad": "NO APLICA",
                    "municipio": "TOLEDO",
                    "tipo_persona": "PREFERENTE",
                    "tipo_cargo": "CANTANTE"
                }
            }
        ]
    }
#