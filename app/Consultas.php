<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultas extends Model
{
    protected $table = 'consultas';
    
    public function usuario()
    {
        return $this->hasOne('\App\User', 'id', 'users_id');
    }
    public function coincidencias()
    {
        return $this->hasMany('\App\PersonasConsulta', 'consultas_id', 'id');
    }
}
