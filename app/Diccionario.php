<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diccionario extends Model
{
    protected $table = 'diccionario';

    public function departamento()
    {
        return $this->hasOne('\App\Departamentos', 'id', 'departamentos_id');
    }
    public function localidad()
    {
        return $this->hasOne('\App\Localidades', 'id', 'localidades_id');
    }
    public function municipio()
    {
        return $this->hasOne('\App\Municipios', 'id', 'municipios_id');
    }
    public function tipo_persona()
    {
        return $this->hasOne('\App\TiposPersona', 'id', 'tipos_persona_id');
    }
    public function tipo_cargo()
    {
        return $this->hasOne('\App\TiposCargo', 'id', 'tipos_cargo_id');
    }
}
