<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposPersona extends Model
{
    protected $table = 'tipos_persona';
}
