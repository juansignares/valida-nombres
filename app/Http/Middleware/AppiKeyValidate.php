<?php

namespace App\Http\Middleware;

use Closure;

class AppiKeyValidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->hasHeader("api-key")) {
            return response()->json([
                'status' => 401,
                'message' => 'Acceso no autorizado: No se detecta Api Key',
            ], 401);
        }

        if ($request->hasHeader("api-key")) {
            $api_key = "WlskIyHmkUfMsyjkTksh";
            if ($request->header("api-key") != $api_key) {
                return response()->json([
                    'status' => 401,
                    'message' => 'Acceso no autorizado - Api Key Incorrecta' ,
                ], 401);
            }
        }
        return $next($request);
    }
}
