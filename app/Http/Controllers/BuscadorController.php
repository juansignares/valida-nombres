<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BuscadorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /* 
     * Index de Cuentas de ahorros del usuario.
     */
    public function index() {
        return view('buscador.index');
    }
}
