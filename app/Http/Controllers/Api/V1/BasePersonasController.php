<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Diccionario;
use App\Departamentos;
use App\Localidades;
use App\Municipios;
use App\TiposCargo;
use App\TiposPersona;
//Job
use App\Jobs\ProcessUploadPersonas;

class BasePersonasController extends Controller
{
    /**
     * Actualizar Base de personas.
     *
     * @return string
     */
    public function updatePersonas()
    {
        $personas = array();
        $header = true;
        $f = fopen(public_path('data/archivoDiccionario.csv'), 'r');
        while ($data = fgetcsv($f, 1000, ";")) {
            if ($header) {
                $header = false;
            } else {
                $personas[] = $data;
            }
        }
        fclose($f);

        //llamar job
        try {
            $job = ProcessUploadPersonas::dispatch($personas)
                ->onConnection('database')
                ->onQueue('procesadorUploadPersonas');
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'No se inició el proceso. Error:' . $e->getMessage()
            ], 401);
        }

        return response()->json([
            'code' => 200,
            'message' => 'Se inició proceso de actualización.'
        ]);
    }
}