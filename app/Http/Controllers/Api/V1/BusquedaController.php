<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Diccionario;
use App\Consultas;
use App\PersonasConsulta;
use Auth;

class BusquedaController extends Controller
{
    /**
     * Nueva Busqueda.
     *
     * @return json
     */
    public function nueva(Request $request)
    {
        $request->validate([
            'busqueda' => 'required|string',
            'porcentaje' => 'required|numeric|min:1|max:100'
        ]);

        $response = array();
        $similitudes = array();

        ////preprocesado input
        //Busqueda
        $busqueda = str_replace(" ", "", $request['busqueda']);
        $busqueda = strtolower($busqueda);
        //Porcentaje
        $porcentaje = floatval($request['porcentaje']);

        // $personas = Diccionario::take(10)->get();
        $personas = Diccionario::all();

        foreach ($personas as $key => $persona) {

            //preprocesado diccionario
            $nom_persona = str_replace(" ", "", $persona->nombre);
            $nom_persona = strtolower($nom_persona);

            //Comparación similitud sencilla
            similar_text($busqueda, $nom_persona, $sim1);

            //Fonética
            similar_text(metaphone($busqueda), metaphone($nom_persona), $sim2);

            //Similitud consolidada
            $sim_final = ($sim1+$sim2)/2;

            if ($sim_final >= $porcentaje) {
                $similitudes[] = [
                    'id' => $persona->id,
                    'nombre_encontrado' => $persona->nombre,
                    'porcentaje_encontrado' => number_format($sim_final, 2, ',', '.'),
                    'otros_campos' => [
                        'anos_activo' => $persona->anos_activo,
                        'departamento' => $persona->departamento->departamento,
                        'localidad' => $persona->localidad->localidad,
                        'municipio' => $persona->municipio->municipio,
                        'tipo_persona' => $persona->tipo_persona->tipo_persona,
                        'tipo_cargo' => $persona->tipo_cargo->tipo_cargo,
                    ],
                ];
            }
        }

        //Inserción de consulta en BD
        $consulta = new Consultas;
        $consulta->users_id = Auth::user()->id;
        $consulta->busqueda = $request['busqueda'];
        $consulta->porcentaje = $porcentaje;
        $consulta->save();
        
        $response = [
            'uuid' => $consulta->id,
            'nombre_buscado' => $request['busqueda'],
            'porcentaje_buscado' => $porcentaje,
            'estado_ejecucion' => (count($similitudes) > 0 ? 'Registros encontrados' : 'Sin coincidencias')
        ];
        

        //Coincidencias
        if (count($similitudes) > 0) {
            $response['coincidencias'] = $similitudes;

            //Inserción de coincidencias en BD
            foreach ($similitudes as $key => $similitud) {
                $coincidencia = new PersonasConsulta;
                $coincidencia->consultas_id = $consulta->id;
                $coincidencia->diccionario_id = $similitud['id'];
                $coincidencia->ptj_coincidencia = floatval($similitud['porcentaje_encontrado']);
                $coincidencia->save();
            }
        }

        return response()->json($response);
    }

    /**
     * Recuperar Busqueda por uuid.
     *
     * @return json
     */
    public function recuperar(Request $request, $uuid)
    {
        $consulta = Consultas::where('id', $uuid)->first();

        if ($consulta) {
            $response = [
                'uuid' => $consulta->id,
                'nombre_buscado' => $consulta->busqueda,
                'porcentaje_buscado' => $consulta->porcentaje
            ];

            if (count($consulta->coincidencias) > 0) {
                $response['estado_ejecucion'] = 'Registros encontrados';
                foreach ($consulta->coincidencias as $key => $coincidencia) {
                    $response['coincidencias'][] = [
                        'id' => $coincidencia->id,
                        'nombre_encontrado' => $coincidencia->persona->nombre,
                        'porcentaje_encontrado' => $coincidencia->ptj_coincidencia,
                        'otros_campos' => [
                            'anos_activo' => $coincidencia->persona->anos_activo,
                            'departamento' => $coincidencia->persona->departamento->departamento,
                            'localidad' => $coincidencia->persona->localidad->localidad,
                            'municipio' => $coincidencia->persona->municipio->municipio,
                            'tipo_persona' => $coincidencia->persona->tipo_persona->tipo_persona,
                            'tipo_cargo' => $coincidencia->persona->tipo_cargo->tipo_cargo,
                        ]
                    ];
                }
            } else {
                $response['estado_ejecucion'] = 'Sin coincidencias';
            }
            
            return response()->json($response);
        } else {
            return response()->json([
                'code' => 400,
                'message' => 'No se encontró el registro de consulta.'
            ], 400);
        }
    }
}