<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonasConsulta extends Model
{
    protected $table = 'personas_consulta';
    
    public function consulta()
    {
        return $this->hasOne('\App\Consultas', 'id', 'consultas_id');
    }
    public function persona()
    {
        return $this->hasOne('\App\Diccionario', 'id', 'diccionario_id');
    }
}
