<?php

use App\Diccionario;
use App\Departamentos;
use App\Localidades;
use App\Municipios;
use App\TiposCargo;
use App\TiposPersona;

if(!function_exists('uploadPersonas')) {
    function uploadPersonas($personas)
    {
        $result = array();
        
        foreach ($personas as $key => $persona) {
            //departamento
            $departamento = Departamentos::where('departamento', $persona[0])->first();
            if (!$departamento) {
                $departamento = new Departamentos;
                $departamento->departamento = $persona[0];
                $departamento->save();
            }

            //localidad
            $localidad = Localidades::where('localidad', $persona[1])->first();
            if (!$localidad) {
                $localidad = new Localidades;
                $localidad->localidad = $persona[1];
                $localidad->save();
            }

            //municipio
            $municipio = Municipios::where('municipio', $persona[2])->first();
            if (!$municipio) {
                $municipio = new Municipios;
                $municipio->municipio = $persona[2];
                $municipio->save();
            }

            //tipo de cargo
            $tipo_cargo = TiposCargo::where('tipo_cargo', $persona[6])->first();
            if (!$tipo_cargo) {
                $tipo_cargo = new TiposCargo;
                $tipo_cargo->tipo_cargo = $persona[6];
                $tipo_cargo->save();
            }

            //tipo de persona
            $tipo_persona = TiposPersona::where('tipo_persona', $persona[5])->first();
            if (!$tipo_persona) {
                $tipo_persona = new TiposPersona;
                $tipo_persona->tipo_persona = $persona[5];
                $tipo_persona->save();
            }


            //persona
            $diccionario = Diccionario::where('nombre', $persona[3])
                ->where('anos_activo', $persona[4])
                ->where('departamentos_id', $departamento->id)
                ->where('localidades_id', $localidad->id)
                ->where('municipios_id', $municipio->id)
                ->where('tipos_persona_id', $tipo_persona->id)
                ->where('tipos_cargo_id', $tipo_cargo->id)
                ->first();
            if (!$diccionario) {
                $diccionario = new Diccionario;
                $diccionario->nombre = $persona[3];
                $diccionario->anos_activo = $persona[4];
                $diccionario->departamentos_id = $departamento->id;
                $diccionario->localidades_id = $localidad->id;
                $diccionario->municipios_id = $municipio->id;
                $diccionario->tipos_persona_id = $tipo_persona->id;
                $diccionario->tipos_cargo_id = $tipo_cargo->id;
                $diccionario->save();
            }
        }
        
        return $result;
    }
}

?>